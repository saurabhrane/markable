package us.markable.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import us.markable.model.Message;
import us.markable.model.Response;
import us.markable.services.MessageService;

@Controller
@RequestMapping(value = "/messages")
public class MessageController {
	
	/** Message Service */
	@Autowired
	MessageService messageService;

	/**
	 * Returns the response with the sum of all amicable numbers which are less than seed if missionId is received for the first time else conflict
	 * 
	 * @param		message		message as a request body with missionId - a unique identifier and seed - a random number between 1000 and 20000
	 * @return					the response with the sum of all amicable numbers which are less than seed if missionId is received for the first time else conflict
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Response> receiveMessage(@RequestBody Message message) {
		int missionId = message.getMissionId();
		int seed = message.getSeed();
		Response response = messageService.sumOfAmicableNumbers(missionId, seed);
		
		if(response == null) {
			return new ResponseEntity<Response>(response, HttpStatus.CONFLICT);
		} else {
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		}
	}
}