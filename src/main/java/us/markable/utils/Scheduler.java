package us.markable.utils;

import java.io.BufferedInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import org.json.JSONObject;
import org.apache.commons.io.IOUtils;

public class Scheduler {
	
	/**
	 * Method which sends messages via HTTP POST requests to /messages every 3 seconds, with a JSON object in the request body.
	 */
	public void run() {
		Random r = new Random();
		int seed = 1000 + r.nextInt(19001);
		int missionId = 1 + r.nextInt(100);
		
		System.out.println("Random");
		System.out.println("Seed : " + seed);
		System.out.println("MissionId: " + missionId);
		
		String uri = "http://localhost:8080/scheduler/messages";
		
		JSONObject message = new JSONObject();
		
		message.put("missionId", missionId);
		message.put("seed", seed);

		String response = "";
		HttpURLConnection conn = null;
		BufferedInputStream in = null;
		OutputStream os = null;

		try {
			String json = message.toString();
			
			URL url = new URL(uri);
			
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			
			os = conn.getOutputStream();
			os.write(json.getBytes("UTF-8"));
			
			in = new BufferedInputStream(conn.getInputStream());
			response = IOUtils.toString(in, "UTF-8");
			
			System.out.println("Response: " + response);
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if(in != null) {
				try { in.close(); } catch(Exception ignore) { }
			}
			if(conn != null) {
				try { conn.disconnect(); } catch(Exception ignore) { }
			}
		}
	}
}
