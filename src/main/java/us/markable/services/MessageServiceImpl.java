package us.markable.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import us.markable.model.Response;

@Service("messageService")
@Transactional
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Returns the answer for given seed
	 * 
	 * @param		missionId	a unique identifier
	 * @param		seed		a random number between 1000 and 20000
	 * @return					the response with the sum of all amicable numbers which are less than seed if missionId is received for the first time else conflict
	 */
	public Response sumOfAmicableNumbers(int missionId, int seed) {
		Response response = null;
		
		String select = "select * from missions where mission_id = ?";
		
		String insert = "insert into missions (mission_id, seed, request_time) values (?, ?, ?)";
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			conn = dataSource.getConnection();

			ps = conn.prepareStatement(select);
			
			ps.setInt(1, missionId);
			
			rs = ps.executeQuery();
			
			if(!rs.next()) {
				if (ps != null) {
					try { ps.close(); } catch (SQLException e) {}
				}

				ps = conn.prepareStatement(insert);
				
				ps.setInt(1, missionId);
				ps.setInt(2, seed);
				ps.setTimestamp(3, new java.sql.Timestamp(new Date().getTime()));
				
				ps.executeUpdate();
				
				int k = sumOfAmicableNumbers(seed);
				response = new Response();
				response.setAnswer(k);	
			}
			
		} catch (Exception e) {
			System.out.println("Message: " + e.getMessage());
		} finally {
			if (rs != null) {
				try { rs.close(); } catch (Exception e) {}
			}
			if (ps != null) {
				try { ps.close(); } catch (Exception e) {}
			}
			if (conn != null) {
				try { conn.close(); } catch (Exception e) {}
			}
		}
		
		return response;
	}

	/**
	 * Returns the sum of all amicable numbers which are less than seed
	 * 
	 * @param		seed	a number between 1000 and 20000
	 * @return				the sum of all amicable numbers which are less than seed
	 */
	public int sumOfAmicableNumbers(int seed) {
		
		int sum = 0;

		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 1; i < seed; i++ ) {
			int a = sumOfFactors(i);
			int b = sumOfFactors(a);
			if(i == b && i != a) {
				if(!list.contains(i)) {
					list.add(i);
				}
				if(!list.contains(b)) {
					list.add(b);
				}
			}
		}		
		
		for(Integer e: list) {
			sum += e;
		}
		
		return sum;
	}
	
	/**
	 * Returns the sum of factors of a number
	 * 
	 * @param		number	a number
	 * @return				the sum of factors of a number
	 */
	public int sumOfFactors(int number) {

		int sum = 0;
		
		for (int div = 1; div <= number/2; div++) {
			if((number % div) == 0) {
				sum += div;
			}
		}
		
		return sum;
	}
	
	@Override
	public String toString() {
		return "us.markable.services.MessageServiceImpl";
	}
}
