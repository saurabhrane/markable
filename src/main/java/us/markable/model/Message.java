package us.markable.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
	
	/** missionId */
	@JsonProperty
	private int missionId;

	/** seed */
	@JsonProperty
	private int seed;
	
	/**
	 * Returns the missionId for the message
	 * 
	 * @return		missionId for the message
	 */
	public int getMissionId() {
		return missionId;
	}
	
	/**
	 * Returns the seed for the message
	 * 
	 * @return		seed for the message
	 */
	public int getSeed() {
		return seed;
	}
}
