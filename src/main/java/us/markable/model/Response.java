package us.markable.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
	
	@JsonProperty
	private int answer;
	
	/**
	 * Returns the answer for the given seed
	 * 
	 * @return		answer for the given seed
	 */
	public void setAnswer(int answer) {
		this.answer = answer;
	}
}
