package us.markable.services;

import us.markable.model.Response;

public interface MessageService {

	Response sumOfAmicableNumbers(int missionId, int seed);
	
	int sumOfFactors(int number);
	
	int sumOfAmicableNumbers(int seed);
}
