package us.markable.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test.xml")
public class MessageServiceImplIntegrationTest {

	@Autowired
	private MessageService messageService;
	
	@Test
	public void testToString() {
		assertEquals("us.markable.services.MessageServiceImpl", messageService.toString());
	}

	@Test
	public void testSumOfFactors() {
		assertEquals(220, messageService.sumOfFactors(284));
		assertEquals(284, messageService.sumOfFactors(220));
	}

	@Test
	public void testSumOfAmicableNumbers() {
		assertEquals(0, messageService.sumOfAmicableNumbers(220));
		assertEquals(220, messageService.sumOfAmicableNumbers(221));
		assertEquals(220, messageService.sumOfAmicableNumbers(284));
		assertEquals(504, messageService.sumOfAmicableNumbers(285));
		assertEquals(504, messageService.sumOfAmicableNumbers(1184));
		assertEquals(1688, messageService.sumOfAmicableNumbers(1185));
		
		assertNotEquals(200, messageService.sumOfAmicableNumbers(250));
		assertEquals(31626, messageService.sumOfAmicableNumbers(10000));		
	}
}
