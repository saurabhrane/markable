# README #

When you deploy this code, the app server starts sending requests to '/messages' every 3 seconds.

Receiving server responds back with answer - (the sum of all amicable numbers which are less than seed from the request) if missionId is not previously received, else responds with conflict message.

* For indempotence, I am storing missionId and seed received in each request in a mysql database; details about it can be gathered from message-servlet.xml file.
* For testing purposes, I am using same database. So, whenever some one runs MessageControllerIntegrationTest, please delete any entry with missionId = 2, otherwise the unit test fails for this class.